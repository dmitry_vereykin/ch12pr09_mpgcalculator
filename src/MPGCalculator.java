/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class MPGCalculator extends JFrame {
    private JButton calculate;
    private JTextField milesFiled;
    private JTextField gallonsField;
    private JTextField mpgField;

    public MPGCalculator() {
        this.setTitle("MPG Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridLayout layout = new GridLayout(3, 2);
        this.setLayout(layout);

        JLabel milesL = new JLabel(" Miles on a full tank");
        milesFiled = new JTextField("0", 10);
        JLabel gallonsL = new JLabel(" The capacity of a tank:");
        gallonsField = new JTextField("0", 10);
        mpgField = new JTextField("0", 10);

        calculate = new JButton("Calculate MPG");
        calculate.addActionListener(new ButtonListener());

        this.add(milesL);
        this.add(milesFiled);
        this.add(gallonsL);
        this.add(gallonsField);
        this.add(calculate);
        this.add(mpgField);

        setSize(300, 130);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            DecimalFormat niceD = new DecimalFormat("0.00");

            try {
                double miles = Double.parseDouble(milesFiled.getText());
                double gallons = Double.parseDouble(gallonsField.getText());
                double mpg = miles / gallons;
                mpgField.setText(niceD.format(mpg));
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "ERROR. Entered value is not a number.");
            }

        }
    }

    public static void main(String[] args) {
        new MPGCalculator();
    }
}
